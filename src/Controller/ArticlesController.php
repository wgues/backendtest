<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\PictureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ArticlesController extends AbstractController
{
    private $entityManager;
    private $articleRepository;

    public function __construct(EntityManagerInterface $entityManager, ArticleRepository $repository) {
        $this->entityManager = $entityManager;
        $this->articleRepository = $repository;
    }
    /**
     * @Route("/articles", name="articles", methods={"GET"})
     */
    public function getArticles(): JsonResponse
    {
        $response = [];
        $articles = $this->articleRepository->findAll();
        foreach ( $articles as $article) {
            $response [] = [ "id" => $article->getId() ,
                             "title" => $article->getTitle(),
                             "content" => $article->getContent(),
                             "picture" => $article->getPicture()->getImageUrl()
                      ];
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/articles/{id}", name="article", methods={"GET"})
     */
    public function getArticle(Article $article): JsonResponse
    {
        $response = [];
        $article [] = [ "id" => $article->getId() ,
            "title" => $article->getTitle(),
            "content" => $article->getContent(),
            "picture" => $article->getPicture()->getImageUrl()
        ];
        return new JsonResponse($article);
    }

    /**
     * @Route("/article/add", name="addArticle", methods={"POST"})
     */
    public function postArticle(Request $request, PictureRepository $pictureRepository): JsonResponse
    {
        $article = new Article();
        $data = json_decode($request->getContent(), true);

        if (empty($data['title'])) {
            return new JsonResponse(['code' => 404, 'message' => 'paramètre title manquant']);
        }
        if (empty($data['slug'])) {
            return new JsonResponse(['code' => 404, 'message' => 'paramètre slug manquant']);
        }
        if (empty($data['picture'])) {
            return new JsonResponse(['code' => 404, 'message' => 'paramètre picture manquant']);
        }
        if (empty($data['content'])) {
            return new JsonResponse(['code' => 404, 'message' => 'paramètre content manquant']);
        }

        $article->setTitle($data['title']);
        $article->setSlug($data['slug']);
        $article->setContent($data['content']);

        $picture = $pictureRepository->find($data['picture']);
        $article->setPicture($picture);

        $this->entityManager->persist($article);
        $this->entityManager->flush();

        return new JsonResponse(['code' => 200, "message" => "success"]);
    }
}
