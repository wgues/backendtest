<?php

namespace App\Controller;

use App\Repository\PictureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class PicturesController extends AbstractController
{

    /**
     * @Route("/pictures", name="pictures", methods={"GET"})
     */
    public function getPictures(PictureRepository $pictureRepository): JsonResponse
    {
        $response = [];
        $pictures = $pictureRepository->findAll();
        foreach ( $pictures as $picture) {
            $response [] = [ "id" => $picture->getId() ,
                "title" => $picture->getTitle(),
                "image" => $picture->getImageUrl()
            ];
        }
        return new JsonResponse($response);
    }
}
